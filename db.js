const sqlite3 = require("sqlite3").verbose();
const filepath = "./manga.db";

function createDbConnection() {
    const db = new sqlite3.Database(filepath, (error) => {
        if (error) {
            return console.error(error.message);
        }
    });
    console.log("Connection with SQLite has been established");
    return db;
}

function createTable(db) {
    db.exec(`
    CREATE TABLE list
    (
      ID INTEGER PRIMARY KEY AUTOINCREMENT,
      name   VARCHAR(50) NOT NULL,
      color   VARCHAR(50) NOT NULL,
      weight INTEGER NOT NULL
    );
  `);
}


module.exports = createDbConnection();