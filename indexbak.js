// const { initializeApp } = require("firebase/app");
// const { getFirestore, addDoc, collection, setDoc, doc } = require("firebase/firestore");
const Crawler = require('crawler');
const axios = require('axios');

// TODO: Replace the following with your app's Firebase project configuration
// See: https://support.google.com/firebase/answer/7015592
// const firebaseConfig = {
//     apiKey: "AIzaSyAFfUYKYs2HgQ7YkoZs0OkVopjBywRDfOs",
//     authDomain: "manga-2bc0a.firebaseapp.com",
//     projectId: "manga-2bc0a",
//     storageBucket: "manga-2bc0a.appspot.com",
//     messagingSenderId: "729919617900",
//     appId: "1:729919617900:web:50fc960291374f51a6d149",
//     measurementId: "G-SJM8PH83HZ"
// };

// Initialize Firebase
// const app = initializeApp(firebaseConfig);


// Initialize Cloud Firestore and get a reference to the service
// const db = getFirestore(app);
async function imageToBase64(imageUrl) {
    try {
        const response = await axios.get(imageUrl, { responseType: 'arraybuffer' });
        const base64Image = Buffer.from(response.data, 'binary').toString('base64');
        return `data:${response.headers['content-type']};base64,${base64Image}`;
    } catch (error) {
        console.error(`Error converting image to base64: ${error}`);
        return null;
    }
}

const c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: (error, res, done) => {
        if (error) {
            console.log(error);
        } else {
            const $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            console.log($('title').text());
        }
        done();
    }
});


c.queue([{
    uri: 'https://kiryuu.id/',
    jQuery: {
        name: 'cheerio',
        options: {
            normalizeWhitespace: true,
            xmlMode: true
        }
    },

    // The global callback won't be called
    callback: async (error, res, done) => {
        if (error) {
            console.log(error);
        } else {
            const $ = res.$;
            const data = [];
            $('div.popconslide>div.bs>div.bsx').each(async (index,element) => {
                const img1 = $(element).find('img').attr('src')
                const img2 = $(element).find('img').attr('data-cfsrc')
                const img = img1 ? img1 : img2
                data.push(img)
            })
            


            console.log(data);
        }
        done();
    }
}]);