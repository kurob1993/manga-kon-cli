const axios = require('axios');

exports.imageToBase64 = async imageUrl => {
    try {
        const response = await axios.get(imageUrl, { responseType: 'arraybuffer' });
        const base64Image = Buffer.from(response.data, 'binary').toString('base64');
        return `data:${response.headers['content-type']};base64,${base64Image}`;
    } catch (error) {
        console.error(`Error converting image to base64: ${error}`);
        return null;
    }
}