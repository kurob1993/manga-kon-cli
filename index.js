const express = require('express')
const cors = require('cors');
const fs = require('fs');
const path = require('path');

const app = express()
const port = 3000

app.use(cors());

// Function to load routes from dynamic folders
const loadRoutes = (dir) => {
  fs.readdirSync(dir).forEach((file) => {
    const fullPath = path.join(dir, file);

    if (fs.statSync(fullPath).isDirectory()) {
      loadRoutes(fullPath); // Recurse into subdirectories
    } else if (file.endsWith('.js')) {
      const route = require(fullPath);
      if (typeof route === 'function' || route.stack) {
        const routePath = `/${path.relative(path.join(__dirname, 'src'), fullPath).replace(/\\/g, '/').replace(/\/routes\/.*$/, '')}`;
        app.use(routePath, route);
      }
    }
  });
};

// Load routes from the src folder
loadRoutes(path.join(__dirname, 'src'));


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})