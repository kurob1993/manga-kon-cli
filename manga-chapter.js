const { initializeApp } = require("firebase/app");
const { getFirestore, addDoc, collection, setDoc, doc, getDocs, query, where } = require("firebase/firestore");
const Crawler = require('crawler');

// TODO: Replace the following with your app's Firebase project configuration
// See: https://support.google.com/firebase/answer/7015592
const firebaseConfig = {
    apiKey: "AIzaSyAFfUYKYs2HgQ7YkoZs0OkVopjBywRDfOs",
    authDomain: "manga-2bc0a.firebaseapp.com",
    projectId: "manga-2bc0a",
    storageBucket: "manga-2bc0a.appspot.com",
    messagingSenderId: "729919617900",
    appId: "1:729919617900:web:50fc960291374f51a6d149",
    measurementId: "G-SJM8PH83HZ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

const c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: (error, res, done) => {
        if (error) {
            console.log(error);
        } else {
            const $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            console.log($('title').text());
        }
        done();
    }
});

const getChapter = async (collectionName, fieldName, searchTerm) => {
    const q = query(collection(db, collectionName), where(fieldName, '>=', searchTerm), where(fieldName, '<=', searchTerm + '\uf8ff'));

    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
    });
}


getChapter('yourCollectionName', 'title', 'naruto');