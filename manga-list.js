const { initializeApp } = require("firebase/app");
const { getFirestore, addDoc, collection, setDoc, doc } = require("firebase/firestore");
const Crawler = require('crawler');

// TODO: Replace the following with your app's Firebase project configuration
// See: https://support.google.com/firebase/answer/7015592
const firebaseConfig = {
    apiKey: "AIzaSyAFfUYKYs2HgQ7YkoZs0OkVopjBywRDfOs",
    authDomain: "manga-2bc0a.firebaseapp.com",
    projectId: "manga-2bc0a",
    storageBucket: "manga-2bc0a.appspot.com",
    messagingSenderId: "729919617900",
    appId: "1:729919617900:web:50fc960291374f51a6d149",
    measurementId: "G-SJM8PH83HZ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

const c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: (error, res, done) => {
        if (error) {
            console.log(error);
        } else {
            const $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            console.log($('title').text());
        }
        done();
    }
});


c.queue([{
    uri: 'https://kiryuu.id/manga/list-mode/',
    jQuery: {
        name: 'cheerio',
        options: {
            normalizeWhitespace: true,
            xmlMode: true
        }
    },

    // The global callback won't be called
    callback: async (error, res, done) => {
        if (error) {
            console.log(error);
        } else {
            const $ = res.$;
            // Extract text and URLs from 'a' tags
            const links = [];
            $('a.series.tip').each(async (index, element) => {
                try {
                    const text = $(element).text().trim();
                    const url = $(element).attr('href');
                    const id = $(element).attr('rel');
                    links.push({ text, url });

                    let data = {
                        title: text,
                        url: url,
                        source: "https://kiryuu.id/manga/list-mode/"
                    }

                    await setDoc(doc(db, "list", id), data);
                } catch (e) {
                    console.error("Error adding document: ", e);
                }

            });

            // Output the extracted links
            console.log(links);
            console.log('Grabbed', res.body.length, 'bytes');

        }
        done();
    }
}]);