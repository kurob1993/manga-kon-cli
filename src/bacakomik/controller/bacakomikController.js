const Crawler = require('crawler');
const image = require('../../../helper/imgaeToBase64')

const base64 = async url => await image.imageToBase64(url)

// Function to fetch Packagist data
exports.allbooks = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: 'https://bacakomik.net/',
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('div.animposx').each(async (index, element) => {
                    // const img1 = $(element).find('img').attr('src')
                    // const img2 = $(element).find('img').attr('data-cfsrc')
                    // const img = img1 ? img1 : img2

                    let img = $(element).find('div.limit>noscript').html()
                    if (img) {
                        const pos1 = img.search('https')
                        const pos2 = img.search('" ')
                        img = img.substring(pos1, pos2)
                    } else {
                        img = ""
                    }

                    const title = $(element).find('img').attr('title')
                    const link = $(element).find('a').attr('href')

                    const lastChapter = $(element).find('div.lsch a').text()
                    const rating = '10'

                    data.push({ title, img, link, lastChapter, rating})
                })

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
};

exports.image = async (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.send(await base64(req.query.url))
}

exports.chapter = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: req.query.url,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = {
                    content: {
                        img: '',
                        desc: '',
                        altname:'',
                        rating: ''
                    },
                    list: []
                }

                const $ = response.$;

                $('div#chapter_list.bxcl.scrolling>ul>li').each(async (index, element) => {
                    const chapter = $(element).find('span.lchx a').text().trim()
                    const date = $(element).find('span.dt a').text()
                    const link = $(element).find('span.lchx a').attr('href')

                    data.list.push({ chapter, link, date })
                })

                let img = $('div.infoanime>div.thumb>noscript').html()
                const pos1 = img.search('https')
                const pos2 = img.search('" ')
                img = img.substring(pos1, pos2)

                data.content.img = img

                $('div.entry-content.entry-content-single p').each((i, el) => {
                    data.content.desc += $(el).text()
                })

                $('div.spe>span>b').remove()
                data.content.altname = $('div.spe>span').first().text().trim()
                data.content.rating = $('div.clearfix.archiveanime-rating i').text().trim()



                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.book = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: req.query.url,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('#anjay_ini_id_kh noscript').each(async (index, element) => {
                    let link = $(element).html()
                    const pos1 = link.search('https')
                    const pos2 = link.search('" ')
                    link = link.substring(pos1, pos2)

                    data.push({ index, link })
                })


                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.search = async (req, res) => {
    const page = req.query.page ? req.query.page : 1
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: `https://bacakomik.net/page/${page}/?s=${req.query.value}`,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = {
                    content: [],
                    pageCount: 0
                }
                const $ = response.$;

                $('div.animepost>div.animposx').each(async (index, element) => {
                    const img1 = $(element).find('img').attr('src')
                    const img2 = $(element).find('img').attr('data-cfsrc')
                    const img = img1 ? img1 : img2

                    const title = $(element).find('h4').text()
                    const link = $(element).find('a').attr('href')

                    data.content.push({ title, img, link })
                })

                const lengthPage = $('div.pagination a').length
                const lastPage = $('div.pagination a')[Number(lengthPage)-2]
                data.pageCount = Number($(lastPage).html())

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.mangalist = async (req, res) => {
    const order = req.query.order ? req.query.order : ''
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: `https://bacakomik.net/daftar-komik/?order=${order}`,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('div.animepost>div.animposx').each(async (index, element) => {
                    const img1 = $(element).find('img').attr('src')
                    const img2 = $(element).find('img').attr('data-cfsrc')
                    const img = img1 ? img1 : img2

                    const title = $(element).find('h4').text()
                    const link = $(element).find('a').attr('href')

                    data.push({ title, img, link })
                })

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

