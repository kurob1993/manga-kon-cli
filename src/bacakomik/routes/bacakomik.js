const express = require('express');
const router = express.Router();
const bacakomikController = require('../controller/bacakomikController');

// Route to fetch Packagist data
router.get('/allbooks', bacakomikController.allbooks );
router.get('/image', bacakomikController.image );
router.get('/chapter', bacakomikController.chapter );
router.get('/book', bacakomikController.book );
router.get('/search', bacakomikController.search );
router.get('/mangalist', bacakomikController.mangalist );

module.exports = router;
