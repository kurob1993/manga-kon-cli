const express = require('express');
const router = express.Router();
const sourceController = require('../controller/sourceController');
const kiryuuController = require('../../kiryuu/controller/kiryuuController');
const bacakomikController = require('../../bacakomik/controller/bacakomikController')

// Route to fetch Packagist data
router.get('/list', sourceController.list );
router.get('/all-books', (req, res) => {
    const source = req.query.source ? req.query.source : null
    if (source === btoa('https://kiryuu.id')) {
        return kiryuuController.allbooks(req, res)
    }

    res.json([])
});

router.get('/chapter', (req, res) => {
    const source = req.query.source ? req.query.source : null
    if (source === btoa('https://kiryuu.id')) {
        return kiryuuController.chapter(req, res)
    } else if (source === btoa('https://bacakomik.net')) {
        return bacakomikController.chapter(req, res)
    }

    res.json([])
});

router.get('/book', (req, res) => {
    const source = req.query.source ? req.query.source : null
    if (source === btoa('https://kiryuu.id')) {
        return kiryuuController.book(req, res)
    } else if (source === btoa('https://bacakomik.net')) {
        return bacakomikController.book(req, res)
    }

    res.json([])
});

router.get('/mangalist', (req, res) => {
    const source = req.query.source ? req.query.source : null
    if (source === btoa('https://kiryuu.id')) {
        return kiryuuController.mangalist(req, res)
    } else if (source === btoa('https://bacakomik.net')) {
        return bacakomikController.mangalist(req, res)
    }

    res.json([])
});

module.exports = router;
