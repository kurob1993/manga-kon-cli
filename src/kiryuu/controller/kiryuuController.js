const Crawler = require('crawler');
const image = require('../../../helper/imgaeToBase64')

const base64 = async url => await image.imageToBase64(url)

// Function to fetch Packagist data
exports.allbooks = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: 'https://kiryuu.id/',
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('div.popconslide>div.bs>div.bsx').each(async (index, element) => {
                    const img1 = $(element).find('img').attr('src')
                    const img2 = $(element).find('img').attr('data-cfsrc')
                    const img = img1 ? img1 : img2

                    const title = $(element).find('img').attr('title')
                    const link = $(element).find('a').attr('href')

                    const lastChapter = $(element).find('div.epxs').text()
                    const rating = $(element).find('div.numscore').text()

                    data.push({ title, img, link, lastChapter, rating})
                })
                
                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
};

exports.image = async (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    res.send(await base64(req.query.url))
}

exports.chapter = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: req.query.url,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = {
                    content: {
                        img: '',
                        desc: '',
                        altname:'',
                        rating: ''
                    },
                    list: []
                }

                const $ = response.$;

                
                $('div#chapterlist>ul.clstyle>li').each(async (index, element) => {
                    const chapter = $(element).find('span.chapternum').html()
                    const date = $(element).find('span.chapterdate').html()
                    const link = $(element).find('a').attr('href')

                    data.list.push({ chapter, link, date })
                })
                const img1 = $('div.thumb>img').attr('src')
                const img2 = $('div.thumb>img').attr('data-cfsrc')
                data.content.img = img1 ?? img2

                $('div.seriestuhead p').each((i, el) => {
                    data.content.desc += $(el).text()
                })

                data.content.altname = $('div.seriestualt').text()
                data.content.rating = $('div.num').text()

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.book = async (req, res) => {
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: req.query.url,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('#readerarea img').each(async (index, element) => {
                    const link = $(element).attr('src')

                    data.push({ index, link })
                })


                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.search = async (req, res) => {
    const page = req.query.page ? req.query.page : 1
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: `https://kiryuu.id/page/${page}/?s=${req.query.value}`,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = {
                    content: [],
                    pageCount: 0
                }
                const $ = response.$;

                $('div.listupd>div.bs>div.bsx').each(async (index, element) => {
                    const img1 = $(element).find('img').attr('src')
                    const img2 = $(element).find('img').attr('data-cfsrc')
                    const img = img1 ? img1 : img2

                    const title = $(element).find('img').attr('title')
                    const link = $(element).find('a').attr('href')

                    data.content.push({ title, img, link })
                })

                const lengthPage = $('div.pagination a').length
                const lastPage = $('div.pagination a')[Number(lengthPage)-2]
                data.pageCount = Number($(lastPage).html())

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

exports.mangalist = async (req, res) => {
    const order = req.query.order ? req.query.order : ''
    const c = new Crawler({
        maxConnections: 10,
    });
    c.queue([{
        uri: `https://kiryuu.id/manga/?order=${order}`,
        jQuery: {
            name: 'cheerio',
            options: {
                normalizeWhitespace: true,
                xmlMode: true
            }
        },

        // The global callback won't be called
        callback: async (error, response, done) => {
            if (error) {
                console.log(error);
            } else {
                const data = []

                const $ = response.$;
                $('div.listupd>div.bs>div.bsx').each(async (index, element) => {
                    const img1 = $(element).find('img').attr('src')
                    const img2 = $(element).find('img').attr('data-cfsrc')
                    const img = img1 ? img1 : img2

                    const title = $(element).find('img').attr('title')
                    const link = $(element).find('a').attr('href')

                    data.push({ title, img, link })
                })

                // console.log(data);
                res.json(data);
            }
            done();
        }
    }]);
}

