const express = require('express');
const router = express.Router();
const kiryuuController = require('../controller/kiryuuController');

// Route to fetch Packagist data
router.get('/allbooks', kiryuuController.allbooks );
router.get('/image', kiryuuController.image );
router.get('/chapter', kiryuuController.chapter );
router.get('/book', kiryuuController.book );
router.get('/search', kiryuuController.search );
router.get('/mangalist', kiryuuController.mangalist );

module.exports = router;
